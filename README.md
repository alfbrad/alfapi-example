# ALF API Example

Example of how to use the ALF API to load a set of contacts into a CSV file
which can then be used to import into a CMS.

Requires ALF Username and API Key.

If you do not have a Key, you can get one using your ALF Username and Password (provided your suscription allows API Access)
by visiting this URL:

https://api.alfinsight.com/Key

You will also need to install the "requests" module, e.g.:

    pip install requests

This script can be run from the command line, passing in ALF username and key:

    python alf_api_downloader.py -u <ALF Username> -k <ALF API Key>

