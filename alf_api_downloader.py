#! python3
# coding: latin-1

"""
Example of how to use the ALF API to load a set of contacts into a CSV file
which can then be used to import into a CMS.

Requires ALF Username and API Key.

If you do not have a Key, you can get one using your ALF Username and Password (provided your suscription allows API Access)
by visiting this URL:

https://api.alfinsight.com/Key

You will also need to install the "requests" module, e.g.:

pip install requests
"""

# Python built-ins
import argparse
import csv
import os

# 3rd party libraries
import requests

class AlfApiODataClient(object):
    """
    Simple ALF API Client
    """
    base_url = "https://api.alfinsight.com"

    def __init__(self, username, key):
        """ init """
        self.username = username
        self.key = key

    @property
    def _key_auth(self):
        """ returns BasicAuthentication tuple for requests.get """
        return (self.username, self.key)

    @property
    def _odata_root(self):
        """ returns root URL for the OData service """
        return "%s/%s" % (self.base_url, "odata")

    def _endpoint_url(self, endpoint, endpoint_id=None, querystring=None):
        """ returns full URL for API query """
        if endpoint_id is not None:
            endpoint += "(%s)" % endpoint_id

        url = "%s/%s" % (self._odata_root, endpoint)

        if querystring:
            url += "?%s" % querystring

        return url

    def _get(self, url):
        """ makes API call and returns data as JSON object """
        response = requests.get(url, auth=self._key_auth)
        response.raise_for_status()

        return response.json()

    def get(self, endpoint, endpoint_id=None, query=None):
        """
        Main API call method. 
        Handles large result sets by querying the API until all results are returned.

        @endpoint: e.g. Contacts/Brands/Companies
        @endpoint_id: endpoints can be queried by ID with calls in format Endpoint(ID)
        @query: any extra OData querystring

        return: generator returning JSON objects
        """
        url = self._endpoint_url(endpoint, endpoint_id, query)
        while url:
            data = self._get(url)
            
            for item in data["value"]:
                yield item

            url = data.get("@odata.nextLink")

    
def download_example(username, key, csv_location=None):
    """
    Example of how ALF API can be used to download data into a CSV file.

    We want to maintain a CSV of contacts with the following filters:

     - Working at companies in the Retail Stores sector
     - Company must have a total rolling advertising spend of over �100,000
     - Must have Marketing Director job function
    """
    client = AlfApiODataClient(username, key)
    endpoint = "Contacts"
   
    filters = [
        "Company/CategoryName eq 'Retail Stores'",
        "Company/Spend/any(s: s/TotalSpend gt 100000 and s/SpendType eq 'Rolling')",
        "JobFunctions/any(j: j/JobFunction eq 'Marketing Director')",
    ]

    # Multiple filters can be added using basic logical operators such as and/or.
    # Here we want all 3 filters so we join each filter with the "and" operator.
    query = "$filter=%s" % " and ".join(filters)
    
    # open our CSV file to export the data
    if not csv_location:
        csv_location = os.path.join(os.path.dirname(__file__), "ALF_Contacts.csv")

    counter = 0
    with open(csv_location, "w", newline='') as csv_file:
        # In this example we are only interested in certain fields:
        field_names = ["PersonId", "JobId", "FirstName", "LastName", "JobTitle", "CompanyId", "CompanyName", "Email", "Telephone", "LinkedInUrl", "DateUpdated"]
        
        # Create CSV writer. The extrasaction="ignore" parameter tells DictWriter not to complain that we are ignoring some fields
        csv_writer = csv.DictWriter(csv_file, field_names, extrasaction="ignore")
        # Write the header row with the column names
        csv_writer.writeheader()

        # Write a row per contact to the CSV file
        for contact in client.get(endpoint, query=query):
            csv_writer.writerow(contact)
            counter += 1

        # and we're done.
        print("Saved %s contacts." % counter)

def run_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--username", type=str, help="ALF Username", dest="username", required=True)
    parser.add_argument("-k", "--key", type=str, help="ALF API Key", dest="key", required=True)
    parser.add_argument("-l", "--location", type=str, help="Alternate location for CSV output", dest="csv_location", required=False)
    args = parser.parse_args()

    download_example(args.username, args.key, args.csv_location)

if __name__ == "__main__":
    run_command_line()
